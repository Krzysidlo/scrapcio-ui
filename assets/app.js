import './styles/app.scss';

import './js/main';
import './bootstrap';
import $ from 'jquery';
import 'bootstrap';
import 'bootstrap-datepicker';
import 'bootstrap-timepicker';

import 'select2';
import 'select2/dist/js/select2.full';

$(document).ready(function () {
    $('[data-toggle="popover"]').popover();
});
