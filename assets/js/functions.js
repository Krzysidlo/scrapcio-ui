let $alertContainer = $('body > .alert-container');

export function toastApiError(data) {
    let defaultMessage = 'Wystąpił błąd przy próbie zapisu.';

    try {
        let errorDetails = JSON.parse(data.responseText),
            message = errorDetails.message || defaultMessage,
            code = errorDetails.code || 500;

        toast(message, code < 500 ? 'warning' : 'danger');
    } catch (error) {
        toast(defaultMessage);
    }
}

export function toast(message, type = 'danger') {
    let $alert = $(`<div class="alert alert-${type}">${message}</div>`);
    $alert.appendTo($alertContainer);
}

export function getBasePath() {
    return window.location.origin + (window.location.pathname.startsWith('/scrapcio') ? '/scrapcio' : '');
}

$alertContainer.on('click', '.alert', (e) => {
    let $alert = $(e.target);

    $alert.fadeOut(() => {
        $alert.remove();
    });
});
