import DataTable from 'datatables.net-bs5';
import {getBasePath} from "./functions";

DataTable;

$(function () {
    let $root = $('body .root');

    if ($root.hasClass('data')) {
        let $table = $root.find('.table'),
            tableName = $table.data('table'),
            originalValue = '';

        async function init() {
            let columnNames = await $.ajax({
                url: `${getBasePath()}/columns/${tableName}`,
                type: 'GET',
            });

            let dataTable = $table.DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: `${getBasePath()}/data/${tableName}`,
                    data: function (d) {
                        return {
                            draw: d.draw,
                            start: d.start,
                            length: d.length,
                            order: d.order,
                            columns: d.columns.map(col => ({name: col.data})),
                            search: d.search.value
                        };
                    },
                    dataSrc: 'data',
                },
                columns: columnNames.map(function (data) {
                    let columnData = {
                            data: data.fieldName,
                            defaultContent: '<input type="text" class="input form-input" disabled placeholder="&lt;null&gt;">',
                            className: ''
                        },
                        isEditable = data.options && data.options.editable;

                    if (data.id) {
                        columnData.className += 'id ';
                    }

                    if (isEditable) {
                        columnData.className += 'editable ';
                    }

                    columnData.render = function (value) {
                        if (null !== value && 'date' === data.type || 'date_pk' === data.type) {
                            value = value.split(' ')[0];
                        }

                        const inputValue = null !== value ? value : '';

                        return `<input type="text" class="input form-input disabled" ${isEditable ? '' : 'readonly'} placeholder="&lt;null&gt;" value="${inputValue}">
<button class="btn btn-sm btn-secondary cancel mt-1"><i class="fa-solid fa-ban"></i></button>
<button class="btn btn-sm btn-primary save mt-1 me-1"><i class="fa-solid fa-floppy-disk"></i></button>`;
                    };

                    return columnData;
                }),
                order: [[1, 'asc']],
                pageLength: 25,
                lengthMenu: [25, 50, 100],
                paging: true,
                scrollX: true,
                scrollY: 'calc(100vh - 310px)',
                language: {
                    url: `${getBasePath()}/datatables.pl.json`
                },
            });

            $table.on('mousedown mouseenter', 'tbody td:not(.editable) .input', (e) => {
                widenInput($(e.target));
            });

            $table.on('mouseleave', 'tbody td:not(.editable) .input:not(:focus)', (e) => {
                shortenInput($(e.target));
            });

            $table.on('blur', 'tbody td:not(.editable) .input', (e) => {
                shortenInput($(e.target));
            });

            $table.on('click', 'tbody td.editable:not(.editing)', (e) => {
                e.stopPropagation();
                $table.find('td.editing .cancel').trigger('click');

                let $cell = $(e.target);

                if (!$cell.is('td')) {
                    $cell = $cell.closest('td');
                }

                let $cellInput = $cell.find('.input');

                originalValue = $cellInput.val();

                $cell.addClass('editing');
                $cellInput.removeClass('disabled');
                $cellInput.focus();
            });

            $table.on('click', 'tbody td.editable .cancel', (e) => {
                let $cell = $(e.target).closest('td'),
                    $cellInput = $cell.find('.input');

                $cellInput.val(originalValue);

                hideEditing($cell);
            });

            $table.on('click', 'tbody td.editable .save', (e) => {
                let $cell = $(e.target).closest('td'),
                    $cellInput = $cell.find('.input'),
                    $row = $cell.closest('tr'),
                    value = $cellInput.val().trim(),
                    cell = dataTable.cell($cell),
                    columnIdx = cell.index().column,
                    columnName = columnNames[columnIdx].fieldName,
                    type = columnNames[columnIdx].type,
                    id = [];

                $row.find('td.id').each((i, e) => {
                    let $cell = $(e),
                        cell = dataTable.cell($cell),
                        columnIdx = cell.index().column;

                    id.push({
                        'name': columnNames[columnIdx].fieldName,
                        'value': $cell.find('.input').val()
                    });
                });

                if (value === '') {
                    value = null;
                }

                $.ajax({
                    url: `${getBasePath()}/update/${tableName}`,
                    type: 'PATCH',
                    data: JSON.stringify({id, columnName, value, type}),
                    dataType: 'json',
                    success: function () {
                        hideEditing($cell);
                    },
                    error: function (xhr, status, error) {
                        console.error('Error updating data:', error);
                    }
                });
            });

            function hideEditing($cell) {
                let $cellInput = $cell.find('.input');

                setTimeout(() => {
                    $cell.removeClass('editing');
                    $cellInput.addClass('disabled');
                }, 1E2);
            }

            function widenInput($input) {
                let width = $input.val().length * 8 + 25;
                $input.attr('style', `width: ${width}px`);
            }

            function shortenInput($input) {
                $input.removeAttr('style');
            }
        }

        init();
    }
});


$.expr[':'].contains = function (a, i, m) {
    return $(a).text().toUpperCase()
        .indexOf(m[3].toUpperCase()) >= 0;
};
