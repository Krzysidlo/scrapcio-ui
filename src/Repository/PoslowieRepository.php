<?php

namespace App\Repository;

use App\Entity\Sejm\Poslowie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Poslowie>
 *
 * @method Poslowie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Poslowie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Poslowie[]    findAll()
 * @method Poslowie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PoslowieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Poslowie::class);
    }
}
