<?php

namespace App\Repository;

use App\Entity\Sejm\PoslowieWykszt;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<PoslowieWykszt>
 *
 * @method PoslowieWykszt|null find($id, $lockMode = null, $lockVersion = null)
 * @method PoslowieWykszt|null findOneBy(array $criteria, array $orderBy = null)
 * @method PoslowieWykszt[]    findAll()
 * @method PoslowieWykszt[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PoslowieWyksztRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PoslowieWykszt::class);
    }
}
