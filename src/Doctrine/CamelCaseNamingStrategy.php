<?php

namespace App\Doctrine;

use Doctrine\ORM\Mapping\NamingStrategy;

class CamelCaseNamingStrategy implements NamingStrategy
{
    public function classToTableName($className): string
    {
        $className = substr($className, strrpos($className, '\\') + 1);
        return lcfirst(preg_replace('/(?<!^)[A-Z]/', '$0', $className));
    }

    public function propertyToColumnName($propertyName, $className = null): string
    {
        return $propertyName;
    }

    public function joinColumnName($propertyName): string
    {
        return $this->propertyToColumnName($propertyName);
    }

    public function referenceColumnName(): string
    {
        return '';
    }

    public function joinTableName($sourceEntity, $targetEntity, $propertyName = null): string
    {
        return $this->classToTableName($sourceEntity) . '_' . $this->classToTableName($targetEntity);
    }

    public function joinKeyColumnName($entityName, $referencedColumnName = null): string
    {
        return $this->classToTableName($entityName) . '_' . ($referencedColumnName ?: $this->referenceColumnName());
    }

    public function embeddedFieldToColumnName(
        $propertyName,
        $embeddedColumnName,
        $classNameName = null,
        $embeddedClassNameName = null
    ): string
    {
        return $propertyName . $embeddedColumnName;
    }
}
