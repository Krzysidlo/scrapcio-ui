<?php

namespace App\Doctrine\Type;
use Doctrine\DBAL\Types\StringType;

class DatePrimaryKeyType extends StringType
{
    const NAME = 'date_pk';
}
