<?php

namespace App\Service;

use App\Exception\ApiException;
use DateTime;
use Doctrine\ORM\Mapping\MappingException;
use Exception;
use RuntimeException;
use Symfony\Component\HttpFoundation\Request;

class DataService extends Service
{
    public function getData(string $db, string $tableName, ?Request $request): ?array
    {
        $repository   = $this->entityManager->getRepository($this->getEntityClassName($db, $tableName));
        $queryBuilder = $repository->createQueryBuilder('t');
        $searchValue  = $request?->get('search') ?? null;

        if (null !== $searchValue) {
            $columns = $this->getColumns($db, $tableName);

            foreach ($columns as $column) {
                $queryBuilder->orWhere("t.{$column['fieldName']} LIKE :search");
            }

            $queryBuilder->setParameter('search', '%' . $searchValue . '%');
        }

        foreach ($request?->get('order') ?? [] as $order) {
            $orderColumn = $order['column'] ?? null;

            if ($orderColumn !== null) {
                $orderDir   = $order['dir'] ?? 'asc';
                $columnName = $request?->get('columns')[$orderColumn]['name'] ?? null;

                if ($columnName !== null) {
                    $queryBuilder->addOrderBy("t.$columnName", $orderDir);
                }
            }
        }

        $recordsTotal      = $repository->count([]);
        $countQueryBuilder = clone $queryBuilder;
        $countQueryBuilder->select('COUNT(t)');
        $recordsFiltered = $countQueryBuilder->getQuery()->getSingleScalarResult();
        $firstResult     = (int)($request?->get('start') ?? 0);
        $maxResults      = $firstResult + ($request?->get('length') ?? 10);

        $queryBuilder->setMaxResults($maxResults)
            ->setFirstResult($firstResult);

        return [
            'draw'            => $request?->query?->getInt('draw', 1) ?? 1,
            'recordsTotal'    => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data'            => $queryBuilder->getQuery()->getResult(),
        ];
    }

    /**
     * @param string $db
     * @param string $tableName
     * @return array
     * @throws MappingException
     */
    public function getColumns(string $db, string $tableName): array
    {
        $metadata = $this->entityManager->getClassMetadata($this->getEntityClassName($db, $tableName));

        return array_map(fn($fieldName) => $metadata->getFieldMapping($fieldName), $metadata->getColumnNames());
    }

    private function getEntityClassName(string $db, string $tableName): string
    {
        $entityClassName = 'App\\Entity\\' . ucfirst($db) . '\\' . ucfirst($tableName);

        if (!class_exists($entityClassName)) {
            throw new RuntimeException("Entity $entityClassName not found.");
        }

        return $entityClassName;
    }

    public function getTables(): array
    {
        return array_map(fn($classMetadata) => $classMetadata->getTableName(), $this->entityManager->getMetadataFactory()->getAllMetadata());
    }

    /**
     * @param string $db
     * @param string $tableName
     * @param Request $request
     * @return void
     * @throws ApiException
     * @throws Exception
     */
    public function updateTable(string $db, string $tableName, Request $request): void
    {
        $data = $request->toArray();
        self::validateRequiredData($data, ['id', 'columnName', 'value', 'type']);

        ['id' => $idColumns, 'columnName' => $columnName, 'value' => $value, 'type' => $type] = $data;

        $repository = $this->entityManager->getRepository($this->getEntityClassName($db, $tableName));
        $primaryKey = [];

        foreach ($idColumns as $idColumn) {
            $primaryKey[$idColumn['name']] = $idColumn['value'];
        }

        $entity = $repository->find($primaryKey);

        if (!$entity) {
            throw new RuntimeException("Entity not found for the given primary key.");
        }

        $setterMethod = 'set' . ucfirst($columnName);

        if (method_exists($entity, $setterMethod)) {
            switch ($type) {
                case 'integer':
                    $value = (int)$value;
                    break;
                case 'date':
                    $value = (new DateTime($value))->setTime(0, 0);
                    break;
                case 'datetime':
                    $value = new DateTime($value);
                    break;
                default:
                    break;
            }

            $entity->$setterMethod($value);

            $this->entityManager->persist($entity);
            $this->entityManager->flush();
        } else {
            throw new RuntimeException("Setter method '$setterMethod' not found for property '$columnName'.");
        }
    }
}
