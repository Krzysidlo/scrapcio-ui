<?php

namespace App\Service;

use App\Exception\ApiException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;

abstract class Service
{
    public function __construct(protected EntityManagerInterface $entityManager)
    {
    }

    /**
     * @param $data
     * @param $requiredData
     * @return void
     * @throws ApiException
     */
    public static function validateRequiredData($data, $requiredData): void
    {
        if (count(array_diff($requiredData, array_keys($data))) > 0) {
            throw new ApiException(
                ApiException::MISSING_DATA,
                Response::HTTP_BAD_REQUEST
            );
        }

    }
}
