<?php

namespace App\Controller;

use App\Exception\ApiException;
use App\Service\DataService;
use Doctrine\ORM\Mapping\MappingException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;

#[Route('', name: 'index')]
class IndexController extends BaseController
{
    #[Route('', name: '_index', methods: [Request::METHOD_GET])]
    public function index(DataService $dataService): Response
    {
        $tables = $dataService->getTables();

        return $this->render('index.html.twig', [
            'tables' => $tables,
        ]);
    }

    /**
     * @param DataService $dataService
     * @param string $tableName
     * @return Response
     * @throws MappingException
     */
    #[Route('/table/{tableName}', name: '_table', methods: [Request::METHOD_GET])]
    public function table(DataService $dataService, string $tableName): Response
    {
        return $this->render('data.html.twig', [
            'tableName'   => $tableName,
            'columnNames' => $dataService->getColumns('sejm', $tableName),
        ]);
    }

    #[Route('/data/{tableName}', name: '_get_data', methods: [Request::METHOD_GET])]
    public function getData(DataService $dataService, string $tableName, ?Request $request = null): JsonResponse
    {
        return $this->json($dataService->getData('sejm', $tableName, $request), Response::HTTP_OK, [], [
            DateTimeNormalizer::FORMAT_KEY => 'Y-m-d H:i:s',
        ]);
    }

    /**
     * @param DataService $dataService
     * @param string $tableName
     * @return JsonResponse
     * @throws MappingException
     */
    #[Route('/columns/{tableName}', name: '_get_columns', methods: [Request::METHOD_GET])]
    public function getColumns(DataService $dataService, string $tableName): JsonResponse
    {
        return $this->json($dataService->getColumns('sejm', $tableName));
    }

    /**
     * @param DataService $dataService
     * @param string $tableName
     * @param Request $request
     * @return JsonResponse
     * @throws ApiException
     */
    #[Route('/update/{tableName}', name: '_update_columns', methods: [Request::METHOD_PATCH])]
    public function update(DataService $dataService, string $tableName, Request $request): JsonResponse
    {
        $dataService->updateTable('sejm', $tableName, $request);

        return (new JsonResponse())->setStatusCode(Response::HTTP_NO_CONTENT);
    }
}
