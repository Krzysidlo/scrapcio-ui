<?php

namespace App\Entity\Sejm;

use App\Doctrine\Type\DatePrimaryKeyType;
use App\Repository\PoslowieFunkcjeRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PoslowieFunkcjeRepository::class)]
class PoslowieFunkcje
{
    #[ORM\Id, ORM\Column]
    private ?int $kadencja = null;

    #[ORM\Id, ORM\Column]
    private ?int $posel = null;

    #[ORM\Id, ORM\Column(length: 60)]
    private ?string $funkcja = null;

    #[ORM\Id, ORM\Column(type: DatePrimaryKeyType::NAME, length: 10)]
    private ?string $dataOd = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?DateTimeInterface $dataDo = null;

    #[ORM\Column(length: 65536, nullable: true, options: ['editable' => true])]
    private ?string $uwagi = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true, options: ['default' => 'CURRENT_TIMESTAMP'])]
    private ?DateTimeInterface $update = null;

    public function getKadencja(): ?int
    {
        return $this->kadencja;
    }

    public function setKadencja(int $kadencja): static
    {
        $this->kadencja = $kadencja;

        return $this;
    }

    public function getPosel(): ?int
    {
        return $this->posel;
    }

    public function setPosel(int $posel): static
    {
        $this->posel = $posel;

        return $this;
    }

    public function getFunkcja(): ?string
    {
        return $this->funkcja;
    }

    public function setFunkcja(string $funkcja): static
    {
        $this->funkcja = $funkcja;

        return $this;
    }

    public function getDataOd(): ?DateTimeInterface
    {
        return DateTime::createFromFormat('Y-m-d', $this->dataOd);
    }

    public function setDataOd(DateTimeInterface $dataOd): static
    {
        $this->dataOd = $dataOd->format('Y-m-d');

        return $this;
    }

    public function getDataDo(): ?DateTimeInterface
    {
        return $this->dataDo;
    }

    public function setDataDo(?DateTimeInterface $dataDo): static
    {
        $this->dataDo = $dataDo;

        return $this;
    }

    public function getUwagi(): ?string
    {
        return $this->uwagi;
    }

    public function setUwagi(?string $uwagi): static
    {
        $this->uwagi = $uwagi;

        return $this;
    }

    public function getUpdate(): ?DateTimeInterface
    {
        return $this->update;
    }

    public function setUpdate(?DateTimeInterface $update): static
    {
        $this->update = $update;

        return $this;
    }
}
