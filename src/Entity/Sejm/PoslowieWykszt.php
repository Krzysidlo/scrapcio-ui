<?php

namespace App\Entity\Sejm;

use App\Repository\PoslowieWyksztRepository;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PoslowieWyksztRepository::class)]
class PoslowieWykszt
{
    #[ORM\Id, ORM\Column]
    private ?int $kadencja = null;

    #[ORM\Id, ORM\Column]
    private ?int $posel = null;

    #[ORM\Id, ORM\Column]
    private ?int $idx = null;

    #[ORM\Column(nullable: true)]
    private ?int $rok = null;

    #[ORM\Column(length: 120, nullable: true)]
    private ?string $uczelnia = null;

    #[ORM\Column(length: 120, nullable: true)]
    private ?string $wydzial = null;

    #[ORM\Column(length: 120, nullable: true)]
    private ?string $kierunek = null;

    #[ORM\Column(length: 120, nullable: true)]
    private ?string $stopien = null;

    #[ORM\Column(length: 1024, nullable: true)]
    private ?string $szkolainfo = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true, options: ['default' => 'CURRENT_TIMESTAMP'])]
    private ?DateTimeInterface $update = null;

    public function getKadencja(): ?int
    {
        return $this->kadencja;
    }

    public function setKadencja(int $kadencja): static
    {
        $this->kadencja = $kadencja;

        return $this;
    }

    public function getPosel(): ?int
    {
        return $this->posel;
    }

    public function setPosel(int $posel): static
    {
        $this->posel = $posel;

        return $this;
    }

    public function getIdx(): ?int
    {
        return $this->idx;
    }

    public function setIdx(int $idx): static
    {
        $this->idx = $idx;

        return $this;
    }

    public function getRok(): ?int
    {
        return $this->rok;
    }

    public function setRok(?int $rok): static
    {
        $this->rok = $rok;

        return $this;
    }

    public function getUczelnia(): ?string
    {
        return $this->uczelnia;
    }

    public function setUczelnia(?string $uczelnia): static
    {
        $this->uczelnia = $uczelnia;

        return $this;
    }

    public function getWydzial(): ?string
    {
        return $this->wydzial;
    }

    public function setWydzial(?string $wydzial): static
    {
        $this->wydzial = $wydzial;

        return $this;
    }

    public function getKierunek(): ?string
    {
        return $this->kierunek;
    }

    public function setKierunek(?string $kierunek): static
    {
        $this->kierunek = $kierunek;

        return $this;
    }

    public function getStopien(): ?string
    {
        return $this->stopien;
    }

    public function setStopien(?string $stopien): static
    {
        $this->stopien = $stopien;

        return $this;
    }

    public function getSzkolainfo(): ?string
    {
        return $this->szkolainfo;
    }

    public function setSzkolainfo(?string $szkolainfo): static
    {
        $this->szkolainfo = $szkolainfo;

        return $this;
    }

    public function getUpdate(): ?DateTimeInterface
    {
        return $this->update;
    }

    public function setUpdate(?DateTimeInterface $update): static
    {
        $this->update = $update;

        return $this;
    }
}
