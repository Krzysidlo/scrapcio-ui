<?php

namespace App\Entity\Sejm;

use App\Repository\PoslowieRepository;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PoslowieRepository::class)]
class Poslowie
{
    #[ORM\Id, ORM\Column]
    private ?int $kadencja = null;

    #[ORM\Id, ORM\Column]
    private ?int $posel = null;

    #[ORM\Column(length: 60, nullable: true)]
    private ?string $imie = null;

    #[ORM\Column(length: 20, nullable: true)]
    private ?string $imieDrugie = null;

    #[ORM\Column(length: 80)]
    private ?string $nazwisko = null;

    #[ORM\Column(nullable: true)]
    private ?int $okreg = null;

    #[ORM\Column(nullable: true)]
    private ?int $lista = null;

    #[ORM\Column(nullable: true)]
    private ?int $miejsce = null;

    #[ORM\Column(nullable: true)]
    private ?int $glosy = null;

    #[ORM\Column(nullable: true)]
    private ?float $pctLista = null;

    #[ORM\Column(nullable: true)]
    private ?float $pctOkreg = null;

    #[ORM\Column(nullable: true)]
    private ?int $rankIntra = null;

    #[ORM\Column(nullable: true)]
    private ?int $rankInter = null;

    #[ORM\Column(nullable: true)]
    private ?float $normRankIntra = null;

    #[ORM\Column(nullable: true)]
    private ?float $normRankInter = null;

    #[ORM\Column(length: 40, nullable: true)]
    private ?string $komitet = null;

    #[ORM\Column(length: 40, nullable: true)]
    private ?string $komSkrot = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true, options: ['comment' => 'data uzyskania mandatu (data wyborów lub data postanowienia o objęciu mandatu)', 'editable' => true])]
    private ?DateTimeInterface $mandatData = null;

    #[ORM\Column(length: 45, nullable: true, options: ['comment' => 'referencja do postanowienia o obsadzeniu mandatu', 'editable' => true])]
    private ?string $mandatRef = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true, options: ['comment' => 'data ślubowania'])]
    private ?DateTimeInterface $slubData = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true, options: ['comment' => 'data wygaśnięcia mandatu'])]
    private ?DateTimeInterface $wygasData = null;

    #[ORM\Column(length: 12, nullable: true, options: ['comment' => 'przyczyna wygaśnięcia mandatu'])]
    private ?string $wygasPrzyczyna = null;

    #[ORM\Column(length: 65536, nullable: true, options: ['comment' => 'przyczyna wygaśnięcia mandatu - opisowo ze strony Sejmu'])]
    private ?string $wygasPrzyczynaOpis = null;

    #[ORM\Column(length: 240, nullable: true, options: ['comment' => 'przyczyna wygaśnięcia mandatu - link do postanowienia Marszałka'])]
    private ?string $wygasDecyzja = null;

    #[ORM\Column(length: 64)]
    private ?string $uuid = null;

    #[ORM\Column(length: 32)]
    private ?string $md5 = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?DateTimeInterface $dataUr = null;

    #[ORM\Column(length: 60)]
    private ?string $miejsceUr = null;

    #[ORM\Column(length: 60, nullable: true)]
    private ?string $nazwiskoRod = null;

    #[ORM\Column(length: 60)]
    private ?string $wykszt = null;

    #[ORM\Column(length: 60, nullable: true)]
    private ?string $stopien = null;

    #[ORM\Column(length: 80, nullable: true)]
    private ?string $kierunek = null;

    #[ORM\Column(length: 320, nullable: true)]
    private ?string $uczelnia = null;

    #[ORM\Column(nullable: true)]
    private ?int $wyksztRok = null;

    #[ORM\Column(length: 120, nullable: true)]
    private ?string $zawod = null;

    #[ORM\Column(nullable: true)]
    private ?int $opi = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $staz = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true, options: ['default' => 'CURRENT_TIMESTAMP'])]
    private ?DateTimeInterface $update = null;

    public function getKadencja(): ?int
    {
        return $this->kadencja;
    }

    public function setKadencja(?int $kadencja): static
    {
        $this->kadencja = $kadencja;

        return $this;
    }

    public function getPosel(): ?int
    {
        return $this->posel;
    }

    public function setPosel(?int $posel): static
    {
        $this->posel = $posel;

        return $this;
    }

    public function getImie(): ?string
    {
        return $this->imie;
    }

    public function setImie(?string $imie): static
    {
        $this->imie = $imie;

        return $this;
    }

    public function getImieDrugie(): ?string
    {
        return $this->imieDrugie;
    }

    public function setImieDrugie(?string $imieDrugie): static
    {
        $this->imieDrugie = $imieDrugie;

        return $this;
    }

    public function getNazwisko(): ?string
    {
        return $this->nazwisko;
    }

    public function setNazwisko(?string $nazwisko): static
    {
        $this->nazwisko = $nazwisko;

        return $this;
    }

    public function getOkreg(): ?int
    {
        return $this->okreg;
    }

    public function setOkreg(?int $okreg): static
    {
        $this->okreg = $okreg;

        return $this;
    }

    public function getLista(): ?int
    {
        return $this->lista;
    }

    public function setLista(?int $lista): static
    {
        $this->lista = $lista;

        return $this;
    }

    public function getMiejsce(): ?int
    {
        return $this->miejsce;
    }

    public function setMiejsce(?int $miejsce): static
    {
        $this->miejsce = $miejsce;

        return $this;
    }

    public function getGlosy(): ?int
    {
        return $this->glosy;
    }

    public function setGlosy(?int $glosy): static
    {
        $this->glosy = $glosy;

        return $this;
    }

    public function getPctLista(): ?float
    {
        return $this->pctLista;
    }

    public function setPctLista(?float $pctLista): static
    {
        $this->pctLista = $pctLista;

        return $this;
    }

    public function getPctOkreg(): ?float
    {
        return $this->pctOkreg;
    }

    public function setPctOkreg(?float $pctOkreg): static
    {
        $this->pctOkreg = $pctOkreg;

        return $this;
    }

    public function getRankIntra(): ?int
    {
        return $this->rankIntra;
    }

    public function setRankIntra(?int $rankIntra): static
    {
        $this->rankIntra = $rankIntra;

        return $this;
    }

    public function getRankInter(): ?int
    {
        return $this->rankInter;
    }

    public function setRankInter(?int $rankInter): static
    {
        $this->rankInter = $rankInter;

        return $this;
    }

    public function getNormRankIntra(): ?float
    {
        return $this->normRankIntra;
    }

    public function setNormRankIntra(?float $normRankIntra): static
    {
        $this->normRankIntra = $normRankIntra;

        return $this;
    }

    public function getNormRankInter(): ?float
    {
        return $this->normRankInter;
    }

    public function setNormRankInter(?float $normRankInter): static
    {
        $this->normRankInter = $normRankInter;

        return $this;
    }

    public function getKomitet(): ?string
    {
        return $this->komitet;
    }

    public function setKomitet(?string $komitet): static
    {
        $this->komitet = $komitet;

        return $this;
    }

    public function getKomSkrot(): ?string
    {
        return $this->komSkrot;
    }

    public function setKomSkrot(?string $komSkrot): static
    {
        $this->komSkrot = $komSkrot;

        return $this;
    }

    public function getMandatData(): ?DateTimeInterface
    {
        return $this->mandatData;
    }

    public function setMandatData(?DateTimeInterface $mandatData): static
    {
        $this->mandatData = $mandatData;

        return $this;
    }

    public function getMandatRef(): ?string
    {
        return $this->mandatRef;
    }

    public function setMandatRef(?string $mandatRef): static
    {
        $this->mandatRef = $mandatRef;

        return $this;
    }

    public function getSlubData(): ?DateTimeInterface
    {
        return $this->slubData;
    }

    public function setSlubData(?DateTimeInterface $slubData): static
    {
        $this->slubData = $slubData;

        return $this;
    }

    public function getWygasData(): ?DateTimeInterface
    {
        return $this->wygasData;
    }

    public function setWygasData(?DateTimeInterface $wygasData): static
    {
        $this->wygasData = $wygasData;

        return $this;
    }

    public function getWygasPrzyczyna(): ?string
    {
        return $this->wygasPrzyczyna;
    }

    public function setWygasPrzyczyna(?string $wygasPrzyczyna): static
    {
        $this->wygasPrzyczyna = $wygasPrzyczyna;

        return $this;
    }

    public function getWygasPrzyczynaOpis(): ?string
    {
        return $this->wygasPrzyczynaOpis;
    }

    public function setWygasPrzyczynaOpis(?string $wygasPrzyczynaOpis): static
    {
        $this->wygasPrzyczynaOpis = $wygasPrzyczynaOpis;

        return $this;
    }

    public function getWygasDecyzja(): ?string
    {
        return $this->wygasDecyzja;
    }

    public function setWygasDecyzja(?string $wygasDecyzja): static
    {
        $this->wygasDecyzja = $wygasDecyzja;

        return $this;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(?string $uuid): static
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getMd5(): ?string
    {
        return $this->md5;
    }

    public function setMd5(?string $md5): static
    {
        $this->md5 = $md5;

        return $this;
    }

    public function getDataUr(): ?DateTimeInterface
    {
        return $this->dataUr;
    }

    public function setDataUr(?DateTimeInterface $dataUr): static
    {
        $this->dataUr = $dataUr;

        return $this;
    }

    public function getMiejsceUr(): ?string
    {
        return $this->miejsceUr;
    }

    public function setMiejsceUr(?string $miejsceUr): static
    {
        $this->miejsceUr = $miejsceUr;

        return $this;
    }

    public function getNazwiskoRod(): ?string
    {
        return $this->nazwiskoRod;
    }

    public function setNazwiskoRod(?string $nazwiskoRod): static
    {
        $this->nazwiskoRod = $nazwiskoRod;

        return $this;
    }

    public function getWykszt(): ?string
    {
        return $this->wykszt;
    }

    public function setWykszt(?string $wykszt): static
    {
        $this->wykszt = $wykszt;

        return $this;
    }

    public function getStopien(): ?string
    {
        return $this->stopien;
    }

    public function setStopien(?string $stopien): static
    {
        $this->stopien = $stopien;

        return $this;
    }

    public function getKierunek(): ?string
    {
        return $this->kierunek;
    }

    public function setKierunek(?string $kierunek): static
    {
        $this->kierunek = $kierunek;

        return $this;
    }

    public function getUczelnia(): ?string
    {
        return $this->uczelnia;
    }

    public function setUczelnia(?string $uczelnia): static
    {
        $this->uczelnia = $uczelnia;

        return $this;
    }

    public function getWyksztRok(): ?int
    {
        return $this->wyksztRok;
    }

    public function setWyksztRok(?int $wyksztRok): static
    {
        $this->wyksztRok = $wyksztRok;

        return $this;
    }

    public function getZawod(): ?string
    {
        return $this->zawod;
    }

    public function setZawod(?string $zawod): static
    {
        $this->zawod = $zawod;

        return $this;
    }

    public function getOpi(): ?int
    {
        return $this->opi;
    }

    public function setOpi(?int $opi): static
    {
        $this->opi = $opi;

        return $this;
    }

    public function getStaz(): ?string
    {
        return $this->staz;
    }

    public function setStaz(?string $staz): static
    {
        $this->staz = $staz;

        return $this;
    }

    public function getUpdate(): ?DateTimeInterface
    {
        return $this->update;
    }

    public function setUpdate(?DateTimeInterface $update): static
    {
        $this->update = $update;

        return $this;
    }
}
