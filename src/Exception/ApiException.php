<?php

namespace App\Exception;

use Exception;

class ApiException extends Exception
{
    const MISSING_DATA = 'Brakuje odpowiednich danych';
}
