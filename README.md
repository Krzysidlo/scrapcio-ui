# pabulib_new

1. You need `.env.local` file with credentials


2. Run a container & install backend packages & migrate database
```
bash docker_run.sh
# needed to click `yes` at the end of installation
```

4. Connect to the container and run front-end installations
```
docker exec -it pabulib_api bash
# and inside container:
npm install && npm run dev
```

If it does not want to refresh then you can
```
# clear cache
bin/console c:c
```
Website on http://localhost:1010

PHP Admin on http://localhost:1000

