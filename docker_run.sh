echo "Building compose..."
docker-compose up -d --build

echo "Instaling front-end packages..."
docker exec -it -u scrapcio scrapcio_ui composer install

#echo "Migrate database..."
#docker exec -it -u scrapcio scrapcio_ui bin/console doctrine:migrations:migrate

echo "Install npm"
docker exec -it -u scrapcio scrapcio_ui npm install